package com.example.cleaningmanagementsystem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class DataAnalytics {

    static class Table{
        public String table_id;
        public DateTime initial_time;
        public DateTime seating_time;
        public DateTime leaving_time;
        public int occupied_minutes_total = 0;
        public int cleaning_minutes_total = 0;
        public int clean_state;  // 0=clean, 1=occupied, 2=needs cleaning

        public Table(String id){
            this.table_id = id;
            this.initial_time = DateTime.now();
            this.clean_state = 0;
        }

        public void seat(){
            this.seating_time = DateTime.now();
            this.clean_state = 1;
        }

        public void purge(){
            this.clean_state = 2;
            this.leaving_time = DateTime.now();
            this.occupied_minutes_total += Minutes.minutesBetween(this.seating_time, DateTime.now()).getMinutes();
        }

        public void clean(){
            this.clean_state = 0;
            this.cleaning_minutes_total += Minutes.minutesBetween(this.leaving_time, DateTime.now()).getMinutes();
        }
    }

    static class TableQueue{
        public int AVE_TABLE_TIME = 20;
        public int STD_TABLE_TIME = 10;
        public int AVE_CLEANING_TIME = 5;
        public int STD_CLEANING_TIME = 2;

        public Queue<Table> serviceQueue;
        public Queue<Table> cleaningQueue;
        public DateTime oldest_seating_time;
        public int open_table_count;

        public TableQueue(int total_tables, Table table){
            table.seat();
            this.serviceQueue = new LinkedList<>();
            this.cleaningQueue = new LinkedList<>();
            serviceQueue.add(table);
            this.oldest_seating_time = table.seating_time;
            this.open_table_count = total_tables - 1;
        }

        public void seatTable(Table table){
            table.seat();
            this.serviceQueue.add(table);
            this.open_table_count -= 1;
        }

        public void unseatTable(Table table){
            try {
                table.purge();
                this.serviceQueue.remove(table);
                this.cleaningQueue.add(table);
                this.oldest_seating_time= this.serviceQueue.peek().seating_time;
            }
            catch (Exception e) {
                System.out.println("Exception: " + e);
            }
        }

        public void cleanTable(Table table){
            try{
                table.clean();
                this.cleaningQueue.remove(table);
                this.open_table_count += 1;
            }
            catch (Exception e) {
                System.out.println("Exception: " + e);
            }
        }

        public int checkCleanQueueTime(){
            if(this.cleaningQueue.isEmpty() == true){
                return 0;
            }
            else{
                Table t = this.cleaningQueue.peek();
                return Minutes.minutesBetween(t.leaving_time, DateTime.now()).getMinutes();
            }
        }

        public ArrayList<Integer> expectedTableTime(){
            ArrayList<Integer> j = new ArrayList<Integer>();
            if(open_table_count == 0){
                if(this.cleaningQueue.isEmpty() == true){
                    int highest_table_mins = Minutes.minutesBetween(this.oldest_seating_time, DateTime.now()).getMinutes();
                    int time_1 = Math.max(0, AVE_TABLE_TIME - highest_table_mins) + AVE_CLEANING_TIME;
                    int time_2 = time_1 + STD_TABLE_TIME + STD_CLEANING_TIME;
                    j.add(time_1);
                    j.add(time_2);
                }
                else{
                    int highest_clean_mins = checkCleanQueueTime();
                    int time_1 = Math.max(0, AVE_CLEANING_TIME - highest_clean_mins);
                    int time_2 = time_1 + STD_CLEANING_TIME;
                    j.add(time_1);
                    j.add(time_2);
                }
            }
            else{
                j.add(0);
                j.add(0);
            }
            return j;
        }

    }

}