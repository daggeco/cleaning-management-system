package com.example.cleaningmanagementsystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class DBHelper extends SQLiteOpenHelper{
    public static final String DATABASE_NAME = "cms.db";
    public static final String TABLE_NAME = "tables";
    public static final String COLUMN_ID = "sensor_id";
    public static final String COLUMN_TABLE_NUM = "table_num";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_SERVER = "server";
    public static final String COLUMN_X_SCREEN_LOC = "x_screen_loc";
    public static final String COLUMN_Y_SCREEN_LOC = "y_screen_loc";
    private HashMap hp;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
            "create table tables " +
            "(sensor_id integer primary key, table_num integer, status integer, server text, x_screen_loc integer, y_screen_loc integer)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    //Add a table into the database
    public boolean insertTable (int sensor_id, int table_num, int status, String server, int x_screen_loc, int y_screen_loc) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("sensor_id", sensor_id);
        contentValues.put("table_num", table_num);
        contentValues.put("status", status);
        contentValues.put("server", server);
        contentValues.put("x_screen_loc", x_screen_loc);
        contentValues.put("y_screen_loc", y_screen_loc);
        db.insert("tables", null, contentValues);
        return true;
    }

    //Get the information for a specific sensor id
    public Cursor getDataID(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from tables where sensor_id=" + id + "", null);
        return res;
    }

    //Get the information for a specific table
    public Cursor getDataTable(int table) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from tables where table_num=" + table + "", null);
        return res;
    }

    //Get the number of rows in the database
    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }

    //update the table that is in the database
    public boolean updateTable (int sensor_id, int table_num, int status, String server, int x_screen_loc, int y_screen_loc) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("sensor_id", sensor_id);
        contentValues.put("table_num", table_num);
        contentValues.put("status", status);
        contentValues.put("server", server);
        contentValues.put("x_screen_loc", x_screen_loc);
        contentValues.put("y_screen_loc", y_screen_loc);
        db.update("tables", contentValues, "sensor_id = ? ", new String[] { Integer.toString(sensor_id) } );
        return true;
    }

    //deletes a table from the database
    public Integer deleteTable (Integer sensor_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("tables",
                "sensor_id = ? ",
                new String[] { Integer.toString(sensor_id) });
    }

    //Get all the tables in the restaurant
    public ArrayList<String> getAllTables() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from tables", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }
        return array_list;
    }
}
